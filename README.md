# login-wiser

Sondagem Técnica e Criativa WISER EDUCAÇÃO - Mobile

Instruções para Instalação e funcionamento

1 - Clonar o repositorio git "https://gitlab.com/rsimplicio/login-wiser.git"

2 - No arquivo "local.properties" que está na pasta "android" atualizar a variável sdk.dir com o caminho da instalação do Sdk da máquina exemplo "/home/rafael/Android/Sdk"

3 - Na raiz do projeto executar no terminal o comando para instalação das dependencias "npm install"

4 - Na raiz do projeto executar no terminal o comando "react-native link" para linkar as dependências e assets como fontes do projeto.

5 - Com um dispositivo conectado ao USB conectado ao computador executar o comando "react-native run-android" para execução do app. 
	OBS.: Para execução do app direto pelo USB é preciso que o modo de "Opções do desenvolvedor" esteja ativo no dispositivo, além da opção de "Depuração USB" ativa.
	OBS.: Caso não esteja com as "Opções do desenvolvedor" ativa, fiz um commit em uma pasta dentro do projeto chamada "apk", e dentro segue um arquivo apk para instalação direta no dispositivo, para instalar é preciso que a opção de instalação para "Fontes desconhecidas" esteja ativada.
	
6 - Ao iniciar o app será apresentada a tela de login, para realizar o login corretamente temos dois usuários: 

	email: simplicio.rsb@gmail.com
	senha: qaz12345
	
	email: jessicagarcez07@gmail.com
	senha: objetivo5
	
