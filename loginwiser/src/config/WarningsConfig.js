import { YellowBox } from 'react-native';

YellowBox.ignoreWarnings([
    'Warning: componentWillUpdate is deprecated', // Removido aviso do componente react-navigation
    'Warning: componentWillMount has been renamed', // Removido aviso do componente react-navigation
    'Warning: ViewPagerAndroid', // Removido aviso do componente react-navigation
    'Warning: componentWillReceiveProps is deprecated', // Removido aviso do componente react-navigation
    'Warning: componentWillReceiveProps has been renamed', // Removido aviso do componente react-navigation
    'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);