import AppConfig from '../AppConfig';
import env from 'react-native-config';

const defaultConfig = { app: '' };
const config = { ...defaultConfig, ...AppConfig };
const versao = env.VERSAO;
const url = env.HOST;

class ServiceConfig {
    static versao = versao;

    static app = config.app || '';

    static url = url;

}

export default ServiceConfig;