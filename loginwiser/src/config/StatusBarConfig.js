import { StatusBar } from 'react-native';
import Theme from '../Theme';

StatusBar.setBackgroundColor(Theme.colors.barColor);
StatusBar.setBarStyle(Theme.colors.barStyle, true);