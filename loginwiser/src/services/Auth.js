import Service from "./Service";

class Auth extends Service {
    static logar = id => this.api.get(`users/${id}`);
}

export default Auth;