import axios from 'axios';
import ServiceConfig from '../config/ServiceConfig';

class Service {
    static api = null;

    static offset = (page, limit) => (page - 1) * limit;

    static create = () => {

        const api = axios.create({
            baseURL: ServiceConfig.url
        });
        
        api.interceptors.request.use(request => {
            request.headers = {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            };
        
            console.log('REQUEST', request);
            return request;
        });
        
        api.interceptors.response.use(response => {
            console.log('RESPONSE', response.data);
            return Promise.resolve(response.data);
        }, error => {
            console.log('RESPONSE error', error.response.data);
            return Promise.reject(error.response.data);
        });

        this.api = api;
    };

    static refresh = () => this.create();
}

export default Service;