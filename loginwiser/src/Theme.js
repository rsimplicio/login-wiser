import { DefaultTheme } from 'react-native-paper';
import Styles from './Styles';

export default {
    ...DefaultTheme,
    roundness: 10,
    colors: {
        ...DefaultTheme.colors,
        primary: '#383E71',
        secondary: '#989FDB',
        text: '#383E71',
        barColor: '#383E71',
        barStyle: 'light-content',
        ...Styles.colors
    },
    fonts: {
        primary: Platform.select({
            ios: 'Montserrat',
            android: 'MontserratLight'
        }),
        primaryItalic: Platform.select({
            ios: 'Montserrat',
            android: 'MontserratLightItalic'
        })
    },
    title: {
        fontSize: 22,
        alignSelf: 'center',
        fontWeight: 'bold',
        marginBottom: 5,
        ...Styles.title
    },
    subtitle: {
        fontSize: 18,
        alignSelf: 'center',
        marginBottom: 5,
        ...Styles.subtitle
    },
    button: {
        success: {
            backgroundColor: '#00bea8',
            color: '#FFF',
            ...(Styles.button ? Styles.button.success : {})
        },
        bottonDegrade: {
            flexDirection: 'row',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            padding: 5,
            borderRadius: 25,
            width: '80%',
            marginBottom: 5,
            marginTop: 50,
            ...(Styles.button ? Styles.button.bottonDegrade : {})
        },
        bottonLabel: {
            color: DefaultTheme.colors.surface,
            textAlign: 'center',
            textTransform: 'uppercase',
            ...(Styles.button ? Styles.button.bottonLabel : {})
        },
        bottonDefalt: {
            color: DefaultTheme.colors.primary,
            textAlign: 'center',
            textTransform: 'uppercase',
            ...(Styles.button ? Styles.button.bottonLabel : {})
        }
    },
    input: {
        default: {
            backgroundColor: '#FFF',
            border: 1,
            ...(Styles.input ? Styles.input.default : {})
        }
    },
};