import React from 'react';
import Routes from './Routes';
import { Provider as PaperProvider } from 'react-native-paper'
import Theme from './Theme';
import { Provider as StoreProvider } from 'react-redux';
import storeConfig from './store/Store';
import 'localstorage-polyfill';
import "./config/StatusBarConfig";
import "./config/WarningsConfig";

export default () =>
  <StoreProvider translucent={true} store={storeConfig()}>
    <PaperProvider theme={Theme}>
      <Routes />
    </PaperProvider>
  </StoreProvider>
