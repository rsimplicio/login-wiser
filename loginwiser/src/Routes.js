import {
    createAppContainer,
    createSwitchNavigator,
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './pages/Login';
import Splash from './pages/Splash';

const PublicNavigator = {
    screen: createStackNavigator(
        {
            Login: createStackNavigator(
                { Home: { screen: Login } },
                {
                    headerMode: 'none',
                    navigationOptions: {
                        headerVisible: false,
                    }
                }
            ),
        },
        {
            headerMode: 'none',
            navigationOptions: {
                headerVisible: false,
            }
        }
    ),
}

const AppNavigator = createSwitchNavigator(
    {
        Splash: { screen: Splash },
        Public: PublicNavigator
    },
    { initialRouteName: 'Splash' }
);

export default createAppContainer(AppNavigator);
