import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native-paper';
import { Image, View } from 'react-native';
import Imagem from '../assets/imgs/servlet.png';
import Service from '../services/Service';
import Theme from '../Theme';

class Splash extends Component {

    componentDidMount() {
        const { navigation } = this.props;

        Service.create();
        navigation.navigate('Public');
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', backgroundColor: Theme.colors.primary }}>
                <Image
                    style={{ width: 100, height: 100 }}
                    source={Imagem}
                />
                <ActivityIndicator animating={true} color={Theme.colors.surface} />
            </View>
        );
    }
}

export default Splash;