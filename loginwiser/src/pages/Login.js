import React, { Component } from 'react';
import { View, StyleSheet, Text, ImageBackground, Keyboard } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import { LinearGradient } from "expo-linear-gradient";
import dataUsers from '../assets/data/users.json';
import EmailTextInput from "../components/input/EmailTextInput";
import PasswordTextInput from "../components/input/PasswordTextInput";
import ButtonPrimary from "../components/buttons/ButtonPrimary";
import Imagem from '../assets/imgs/background.jpeg';
import Alert from '../helper/Alert';
import Auth from '../services/Auth';
import Theme from '../Theme';

class Login extends Component {

    state = {
        users: [],
        password: null,
        email: null,
        errorEmail: false,
        tecladoAtivo: false,
        loading: false
    };

    componentDidMount() {
        this.carregaUsers();
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    carregaUsers = () => {
        let users = dataUsers;
        this.setState({ users });
    }

    _keyboardDidShow = () => {
        let tecladoAtivo = true;
        this.setState({ tecladoAtivo });
    }

    _keyboardDidHide = () => {
        let tecladoAtivo = false;
        this.setState({ tecladoAtivo });
    }

    callbackEmail = (email, errorEmail) => {
        this.setState({
            email: email,
            errorEmail: errorEmail
        });
    };

    callbackPass = (password, errorPassword) => {
        this.setState({
            password: password,
            errorPassword: errorPassword
        });
    };

    login = () => {
        const { users, email, password } = this.state;

        this.setState({ loading: true });

        if (!email) {
            this.setState({ errorEmail: true, loading: false });
        }

        if (!password) {
            this.setState({ errorPassword: true, loading: false });
        }

        let userId = null;

        users.forEach(user => {
            if (user.email === email && user.password === password) {
                userId = user.id;
                return;
            }
        });

        if (userId) {
            Auth.logar(userId).then(resp => {
                Alert.aviso(`Parabéns, você está logado como ${resp.name}, email: ${resp.email}`);
                this.setState({ loading: false });
            }).catch(error => {
                this.setState({ loading: false });
                console.log(error);
            });
        } else {
            this.setState({ loading: false });
            Alert.aviso(`Email ou senha inválidos.`);
        }
    };

    render() {
        const { loading, tecladoAtivo, email, errorEmail, password, errorPassword } = this.state;

        if(loading){
            return <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator animating={true} color={Theme.colors.primary}  />
                   </View>
        }

        return (
            <View style={style.container}>
                <ImageBackground source={Imagem} style={style.imgBackground}>
                    <LinearGradient
                        colors={['rgba(105, 57, 153, 0) 100%)', '#130525']}
                        start={{ x: 0, y: 0 }}
                        end={{ x: 0, y: 0.5 }}
                        style={style.linearGradient}
                    >
                        <View style={tecladoAtivo ? style.viewBoxKeyboardDidShow : style.viewBox}>
                            <Text style={style.textBox}>
                                Olá, seja{'\n'} bem-vindo!
                        </Text>
                            <Text style={style.subTextBox}>
                                Para acessar a plataforma, faça seu login.
                        </Text>

                            <EmailTextInput
                                email={email}
                                callbackEmail={this.callbackEmail}
                                error={errorEmail}
                            ></EmailTextInput>

                            <PasswordTextInput
                                password={password}
                                callbackPass={this.callbackPass}
                                error={errorPassword}
                            ></PasswordTextInput>

                            <ButtonPrimary action={() => this.login()} error={errorPassword} >
                                ENTRAR
                            </ButtonPrimary>
                        </View>

                        <View style={style.viewText}>
                            <Text style={style.textPass}>
                                Esqueceu seu login ou senha?{'\n'} Clique&nbsp;
                            <Text style={{ textDecorationLine: 'underline' }}>aqui</Text>
                            </Text>
                        </View>

                    </LinearGradient>
                </ImageBackground>
            </View>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        // alignItems: "center",
        justifyContent: "center",
    },
    imgBackground: {
        flex: 1,
        width: "100%",
        height: 360,
        alignItems: "center",
    },
    linearGradient: {
        width: '100%',
        height: '100%',
        opacity: 1,
        justifyContent: 'center',
        // alignItems: 'center'
    },
    viewBoxKeyboardDidShow: {
        backgroundColor: '#FAF5FF',
        alignSelf: 'center',
        alignContent: 'center',
        width: '80%',
        height: '90%',
        borderRadius: 8
    },
    viewBox: {
        backgroundColor: '#FAF5FF',
        alignSelf: 'center',
        alignContent: 'center',
        width: '80%',
        height: '55%',
        borderRadius: 8
    },
    viewText: {
        // marginTop: '20%'
    },
    textBox: {
        color: Theme.colors.primary,
        fontFamily: Theme.fonts.primary,
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 24,
        textAlign: 'center',
        marginTop: '10%',
        lineHeight: 32
    },
    subTextBox: {
        color: Theme.colors.secondary,
        fontFamily: Theme.fonts.primary,
        fontWeight: '600',
        fontStyle: 'normal',
        fontSize: 12,
        textAlign: 'center',
        marginTop: '5%',
        lineHeight: 20
    },
    textPass: {
        color: 'white',
        fontFamily: Theme.fonts.primary,
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 14,
        textAlign: 'center',
        marginTop: '15%',
        lineHeight: 20
    }
});

export default Login;