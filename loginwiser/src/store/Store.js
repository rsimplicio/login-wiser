import { createStore, combineReducers } from 'redux';

const reducers = combineReducers({
    
});

const storeConfig = () => createStore(reducers);

export default storeConfig;