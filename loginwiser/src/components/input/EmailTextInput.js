import { StyleSheet, Text, TextInput, View } from "react-native";
import Theme from "../../Theme";
import React, { Component } from "react";
import { ValidateRequiredField } from "../warning/validateRequiredField";
import EmailUtils from "../../utils/EmailUtils";

class EmailTextInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: props.email,
            emailValido: true
        }
    }

    verificarEmail = (email) => {
        let result = EmailUtils.validarEmail(email);
        this.setState({
            email: email,
            emailValido: result
        })

        this.props.callbackEmail(email, !result);
    }

    render() {
        return (
            <View style={ styles.viewInput }>
                <Text style={styles.titleInput}>E-MAIL</Text>
                <TextInput
                    ref={this.state.email}
                    autoCorrect={false}
                    autoCapitalize='none'
                    style={Platform.OS === 'ios' ? (this.state.emailValido ? styles.inputIOS : styles.inputIOSError) : (this.state.emailValido ? styles.inputAndroid : styles.inputAndroidError)}
                    blurOnSubmit={false}
                    value={this.state.email}
                    onChangeText={email => this.verificarEmail(email)}
                />
                <ValidateRequiredField value={this.props.error} />
                {
                    !this.state.emailValido ?
                        <Text style={ styles.errorText }>
                            Digite um e-mail válido;
                        </Text> :
                        <View></View>
                }
            </View>

        );
    }
}

const styles = StyleSheet.create({
    viewInput: {
        marginTop: 15,
        marginLeft: 30,
        marginRight: 30,
    },
    titleInput: {
        color: Theme.colors.primary,
        fontFamily: Theme.fonts.primary,
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 10,
        marginTop: '5%',
        lineHeight: 10
    },
    errorText: {
        fontFamily: Theme.fonts.primary, 
        fontSize: 14, 
        color: '#FF377F', 
        marginLeft: 5, 
        marginTop: '1%'
    },
    inputIOS: {
        marginTop: 10,
        backgroundColor: 'transparent',
        fontSize: 16,
        borderRadius: 8,
        borderColor: '#989FDB',
        color: '#989FDB',
        height: 50,
    },
    inputIOSError: {
        marginTop: 10,
        backgroundColor: 'transparent',
        fontSize: 16,
        borderRadius: 8,
        borderColor: '#FF377F',
        color: '#989FDB',
        height: 50,
    },
    inputAndroid: {
        marginTop: 10,
        backgroundColor: 'transparent',
        fontSize: 16,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#989FDB',
        color: '#989FDB',
        height: 50,
    },
    inputAndroidError: {
        marginTop: 10,
        backgroundColor: 'transparent',
        fontSize: 16,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#FF377F',
        color: '#989FDB',
        height: 50,
    },
});

export default EmailTextInput;
