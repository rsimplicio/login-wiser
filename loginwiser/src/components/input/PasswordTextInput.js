import { StyleSheet, Text, TextInput, View } from "react-native";
import Theme from "../../Theme";
import React, { Component } from "react";
import { ValidateRequiredField } from "../warning/validateRequiredField";

class PasswordTextInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            password: props.password
        }
    }

    verificarPassword = (password) => {
        this.setState({
            password: password
        })

        this.props.callbackPass(password, false);
    }

    render() {
        return (
            <View style={ styles.viewInput }>
                <Text style={styles.titleInput}>SENHA</Text>
                <TextInput
                    ref={this.state.password}
                    autoCorrect={false}
                    autoCapitalize='none'
                    style={Platform.OS === 'ios' ? styles.inputIOS : styles.inputAndroid}
                    blurOnSubmit={false}
                    value={this.state.password}
                    secureTextEntry={true}
                    onChangeText={password => this.verificarPassword(password)}
                />
                <ValidateRequiredField value={this.props.error} />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    viewInput: {
        marginTop: 15,
        marginLeft: 30,
        marginRight: 30,
    },
    titleInput: {
        color: Theme.colors.primary,
        fontFamily: Theme.fonts.primary,
        fontWeight: '400',
        fontStyle: 'normal',
        fontSize: 10,
        marginTop: '5%',
        lineHeight: 10
    },
    inputIOS: {
        marginTop: 10,
        backgroundColor: 'transparent',
        fontSize: 16,
        borderRadius: 8,
        borderColor: '#989FDB',
        color: '#989FDB',
        height: 50,
    },
    inputAndroid: {
        marginTop: 10,
        backgroundColor: 'transparent',
        fontSize: 16,
        borderWidth: 1,
        borderRadius: 8,
        borderColor: '#989FDB',
        color: '#989FDB',
        height: 50,
    }
});

export default PasswordTextInput;
