import {Text, View} from "react-native";
import Theme from "../../Theme";
import React from "react";

export const ValidateRequiredField = (props) => (
    props.value ?
        <Text style={{ fontFamily: Theme.fonts.primary, fontSize: 14, color: '#FF377F', marginLeft: 5 }}>
            Preenchimento obrigatório.
        </Text> :
        <View></View>

);