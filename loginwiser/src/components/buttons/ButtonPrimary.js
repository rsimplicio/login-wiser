import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import Theme from "../../Theme";

class BotaoPrimario extends Component {
  render() {
    return (
      <TouchableOpacity
        style={{
          ...this.props.buttonStyle,
        }}
        onPress={() => {
          if (this.props.action) {
            this.props.action();
          } else {
            console.log("action não encontrado");
          }
        }}
        underlayColor="#fff"
        disabled={this.props.disabled}
      >
        <LinearGradient start={[0, 0.5]}
          end={[1, 0.5]}
          colors={['#383E71', '#9D25B0']}
          error={this.props.error}
          style={this.props.error ? style.buttonError : style.button}>
          <Text
            style={{
              fontFamily: Theme.fonts.primary,
              fontWeight: "normal",
              fontSize: this.props.fontSize || 16,
              lineHeight: 20,
              color: "white",
              alignSelf: "center",
              ...this.props.textStyle,
            }}
          >
            {this.props.children}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}

const style = StyleSheet.create({
  circleGradient: {
    margin: 1,
    backgroundColor: "white",
    borderRadius: 5
  },
  button: {
    minWidth: "55%",
    padding: 15,
    marginTop: 40,
    alignSelf: "center",
    borderRadius: 8
  },
  buttonError: {
    minWidth: "55%",
    padding: 15,
    marginTop: 8,
    alignSelf: "center",
    borderRadius: 8
  }
});

export default BotaoPrimario;
